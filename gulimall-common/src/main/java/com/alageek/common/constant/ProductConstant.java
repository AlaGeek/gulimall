package com.alageek.common.constant;

public class ProductConstant {

    public enum AttrEnum {
        ATTR_TYPE_BASE(1, "base", "基本属性"),
        ATTR_TYPE_SALE(0, "sale", "销售属性");

        private final int code;
        private final String type;
        private final String msg;

        AttrEnum(int code, String type, String msg) {
            this.code = code;
            this.type = type;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getType() {
            return type;
        }

        public String getMsg() {
            return msg;
        }
    }

    public enum StatusAttrEnum {
        SPU_NEW(0, "新建"),
        SPU_UP(1, "上架"),
        SPU_DOWN(2, "下架");

        private final int code;
        private final String msg;

        StatusAttrEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }

}
