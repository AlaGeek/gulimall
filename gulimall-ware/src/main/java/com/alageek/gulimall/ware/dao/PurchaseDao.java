package com.alageek.gulimall.ware.dao;

import com.alageek.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author alageek
 * @email feishi_abc@163.com
 * @date 2021-08-26 11:25:19
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
