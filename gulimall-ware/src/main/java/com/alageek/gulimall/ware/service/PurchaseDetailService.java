package com.alageek.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.alageek.common.utils.PageUtils;
import com.alageek.gulimall.ware.entity.PurchaseDetailEntity;

import java.util.Map;

/**
 * 
 *
 * @author alageek
 * @email feishi_abc@163.com
 * @date 2021-08-26 11:25:19
 */
public interface PurchaseDetailService extends IService<PurchaseDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageByCondition(Map<String, Object> params);
}

