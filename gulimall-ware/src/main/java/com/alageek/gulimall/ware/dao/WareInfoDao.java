package com.alageek.gulimall.ware.dao;

import com.alageek.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author alageek
 * @email feishi_abc@163.com
 * @date 2021-08-26 11:25:19
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
