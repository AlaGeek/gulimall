package com.alageek.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.alageek.common.utils.PageUtils;
import com.alageek.gulimall.ware.entity.WareOrderTaskEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author alageek
 * @email feishi_abc@163.com
 * @date 2021-08-26 11:25:19
 */
public interface WareOrderTaskService extends IService<WareOrderTaskEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

