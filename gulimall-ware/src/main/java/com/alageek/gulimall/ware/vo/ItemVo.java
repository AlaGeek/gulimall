package com.alageek.gulimall.ware.vo;

import lombok.Data;

@Data
public class ItemVo {

    private Long itemId;

    private Integer status;

    private String reason;

}
