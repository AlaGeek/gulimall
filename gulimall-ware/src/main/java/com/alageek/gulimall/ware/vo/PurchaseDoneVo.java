package com.alageek.gulimall.ware.vo;

import com.alageek.common.constant.WareConstant;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class PurchaseDoneVo {

    @NotNull
    private Long id;

    private List<ItemVo> items;

}
