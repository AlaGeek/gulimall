package com.alageek.gulimall.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class AttrGroupRelationVo {

    /**
     * 属性id
     */
    private Long attrId;

    /**
     * 分组id
     */
    private Long attrGroupId;

}
