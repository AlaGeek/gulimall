package com.alageek.gulimall.product.dao;

import com.alageek.gulimall.product.entity.SpuInfoDescEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息介绍
 * 
 * @author alageek
 * @email feishi_abc@163.com
 * @date 2021-08-25 23:30:47
 */
@Mapper
public interface SpuInfoDescDao extends BaseMapper<SpuInfoDescEntity> {
	
}
