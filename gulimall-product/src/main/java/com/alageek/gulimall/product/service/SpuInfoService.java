package com.alageek.gulimall.product.service;

import com.alageek.gulimall.product.vo.SpuSaveVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.alageek.common.utils.PageUtils;
import com.alageek.gulimall.product.entity.SpuInfoEntity;

import java.util.Map;

/**
 * spu信息
 * @author alageek
 * @email feishi_abc@163.com
 * @date 2021-08-25 23:30:47
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuInfo(SpuSaveVo spuSaveVo);

    PageUtils queryPageByCondition(Map<String, Object> params);

    void up(Long spuId);
}

