package com.alageek.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.alageek.common.utils.PageUtils;
import com.alageek.gulimall.coupon.entity.MemberPriceEntity;

import java.util.Map;

/**
 * 商品会员价格
 *
 * @author alageek
 * @email feishi_abc@163.com
 * @date 2021-08-26 11:20:17
 */
public interface MemberPriceService extends IService<MemberPriceEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

