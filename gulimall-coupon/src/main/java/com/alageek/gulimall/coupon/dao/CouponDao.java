package com.alageek.gulimall.coupon.dao;

import com.alageek.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author alageek
 * @email feishi_abc@163.com
 * @date 2021-08-26 11:20:17
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
