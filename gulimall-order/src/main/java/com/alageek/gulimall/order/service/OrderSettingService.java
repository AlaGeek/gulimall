package com.alageek.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.alageek.common.utils.PageUtils;
import com.alageek.gulimall.order.entity.OrderSettingEntity;

import java.util.Map;

/**
 * 订单配置信息
 *
 * @author alageek
 * @email feishi_abc@163.com
 * @date 2021-08-26 11:16:09
 */
public interface OrderSettingService extends IService<OrderSettingEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

