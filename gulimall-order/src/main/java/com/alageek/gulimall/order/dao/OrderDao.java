package com.alageek.gulimall.order.dao;

import com.alageek.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author alageek
 * @email feishi_abc@163.com
 * @date 2021-08-26 11:16:09
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
